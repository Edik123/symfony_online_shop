TEST = docker-compose run --rm php-cli php bin/phpunit
NODE = docker-compose run --rm php-cli node_modules
VENDOR = docker-compose run --rm php-cli vendor

up: docker-up
down: docker-down
restart: docker-down docker-up
init: docker-down-clear docker-build docker-up app-init test-env

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build

cli:
	docker-compose run --rm php-cli php bin/console

test: update-test-env
	${TEST}

test-unit:
	${TEST} --testsuite=unit

test-functional:
	${TEST} --testsuite=functional

test-integration:
	${TEST} --testsuite=integration

run-test:
	sh ./app/bin/run-tests.sh

app-init: composer-install createdb migrations

test-env: createdb-test test-migrations fixtures

update-test-env: updatedb-test test-migrations fixtures xvfb detect-drivers

composer-install:
	docker-compose run --rm php-cli composer install --ignore-platform-reqs

migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --no-interaction

test-migrations:
	docker-compose run --rm php-cli php bin/console doctrine:migrations:migrate --env="test" --no-interaction

fixtures:
	docker-compose run --rm php-cli php bin/console doctrine:fixtures:load --env="test" --no-interaction

createdb:
	docker-compose run --rm php-cli php bin/console doctrine:database:create

createdb-test:
	docker-compose run --rm php-cli php bin/console doctrine:database:create --env="test"

updatedb-test:
	docker-compose run --rm php-cli php bin/console doctrine:database:drop --force --env="test"
	docker-compose run --rm php-cli php bin/console doctrine:database:create --env="test"

detect-drivers:
	docker-compose run --rm php-cli vendor/bin/bdi detect drivers

xvfb:
	docker-compose run --rm php-cli Xvfb -ac :20 -screen 0 1280x1024x16 & export DISPLAY=:20

eslint:
	${NODE}/.bin/eslint assets/js/ --ext .js,.vue --fix

php-cs-fixer:
	${VENDOR}/bin/php-cs-fixer fix src --verbose

phpstan:
	${VENDOR}/bin/phpstan analyse src --level 8
