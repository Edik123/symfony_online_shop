<?php

namespace App\Security;

use App\Entity\User;
use App\Services\PasswordGenerator;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class InstagramAuthenticator extends OAuth2Authenticator
{
    private $passwordEncoder;

    private $clientRegistry;

    private $urlGenerator;

    private $router;
    
    private $em;

    private $session;

    private MailerInterface $mailer;

    public function __construct(
        UserPasswordHasherInterface $passwordEncoder,
        UrlGeneratorInterface $urlGenerator,
        ClientRegistry $clientRegistry,
        RouterInterface $router, 
        EntityManagerInterface $em,
        SessionInterface $session,
        MailerInterface $mailer
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->clientRegistry = $clientRegistry;
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->em = $em;
    }

    public function supports(Request $request): ?bool
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_instagram_check';
    }

    public function authenticate(Request $request): PassportInterface
    {
        $client = $this->clientRegistry->getClient('instagram');
        $accessToken = $this->fetchAccessToken($client);

        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), function() use ($accessToken, $client) {
                /** @var InstagramUser $instagramUser */
                $instagramUser = $client->fetchUserFromToken($accessToken);

                $name = $instagramUser->getNickname(); 

                // 1) have they logged in with Instagram before? Easy!
                $existingUser = $this->em->getRepository(User::class)->findOneBy(['instagramId' => $instagramUser->getId()]);

                if ($existingUser) {
                    return $existingUser;
                }

                // 2) do we have a matching user by name?
                $user = $this->em->getRepository(User::class)->findOneBy(['fullName' => $name]);
                
                if (!$user){
                    $user = new User();
                    
                    $user->setInstagramId($instagramUser->getId());
                    $user->setFullName($instagramUser->getNickname());
                    $user->setEmail($instagramUser->getNickname().'@mail.ru');
                    $user->setIsVerified(true);
                    
                    $plainPassword = PasswordGenerator::generatePassword();
                    $encodedPassword = $this->passwordEncoder->hashPassword($user, $plainPassword);
                    $user->setPassword($encodedPassword);

                    $this->em->persist($user);

                    $this->session->getFlashBag()->add(
                        'success', 
                        "You email <b>{$user->getEmail()}</b> and password <b>{$plainPassword}</b>"
                    );
                } else {
                    $user->setInstagramId($instagramUser->getId());
                    $this->em->persist($user);
                }
                
                $this->em->flush();

                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('profile_index');

        return new RedirectResponse($targetUrl);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            '/connect/', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}