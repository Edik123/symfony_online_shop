<?php

namespace App\Security;

use App\Entity\User;
use App\Services\PasswordGenerator;
use Symfony\Component\Mime\Address;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use KnpU\OAuth2ClientBundle\Security\Authenticator\OAuth2Authenticator;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\PassportInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class FacebookAuthenticator extends OAuth2Authenticator
{
    private $passwordEncoder;

    private $clientRegistry;

    private $urlGenerator;

    private $router;
    
    private $em;

    private $session;

    private MailerInterface $mailer;

    public function __construct(
        UserPasswordHasherInterface $passwordEncoder,
        UrlGeneratorInterface $urlGenerator,
        ClientRegistry $clientRegistry,
        RouterInterface $router, 
        EntityManagerInterface $em,
        SessionInterface $session,
        MailerInterface $mailer
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->clientRegistry = $clientRegistry;
        $this->urlGenerator = $urlGenerator;
        $this->session = $session;
        $this->mailer = $mailer;
        $this->router = $router;
        $this->em = $em;
    }

    public function supports(Request $request): ?bool
    {
        // continue ONLY if the current ROUTE matches the check ROUTE
        return $request->attributes->get('_route') === 'connect_facebook_check';
    }

    public function authenticate(Request $request): PassportInterface
    {
        $client = $this->clientRegistry->getClient('facebook_main');
        $accessToken = $this->fetchAccessToken($client);

        return new SelfValidatingPassport(
            new UserBadge($accessToken->getToken(), function() use ($accessToken, $client) {
                /** @var FacebookUser $facebookUser */
                $facebookUser = $client->fetchUserFromToken($accessToken);

                $email = $facebookUser->getEmail();

                // 1) have they logged in with Facebook before? Easy!
                $existingUser = $this->em->getRepository(User::class)->findOneBy(['facebookId' => $facebookUser->getId()]);

                if ($existingUser) {
                    return $existingUser;
                }

                // 2) do we have a matching user by email?
                $user = $this->em->getRepository(User::class)->findOneBy(['email' => $email]);
                
                if (!$user){
                    $user = new User();
                    
                    $user->setFacebookId($facebookUser->getId());
                    $user->setEmail($facebookUser->getEmail());
                    $user->setFullName($facebookUser->getName());
                    $user->setIsVerified(true);
                    
                    $plainPassword = PasswordGenerator::generatePassword();
                    $encodedPassword = $this->passwordEncoder->hashPassword($user, $plainPassword);
                    $user->setPassword($encodedPassword);

                    $this->em->persist($user);

                    $this->mailer->send((new TemplatedEmail())
                            ->from(new Address('ed_va@mail.ru', 'Ed'))
                            ->to($user->getEmail())
                            ->subject('Your new password')
                            ->context([
                                'user' => $user,
                                'plainPassword' => $plainPassword,
                                'profileUrl' => $this->urlGenerator->generate('profile_index', [], UrlGeneratorInterface::ABSOLUTE_URL)
                            ])
                            ->htmlTemplate('registration/password_letter.html.twig')
                    );

                    $this->session->getFlashBag()->add(
                        'success', 
                        'An email has been sent. Please check your inbox to find password'
                    );
                } else {
                    $user->setFacebookId($facebookUser->getId());
                    $this->em->persist($user);
                }
                
                $this->em->flush();

                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        // change "app_homepage" to some route in your app
        $targetUrl = $this->router->generate('profile_index');

        return new RedirectResponse($targetUrl);
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $message = strtr($exception->getMessageKey(), $exception->getMessageData());

        return new Response($message, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication is needed, but it's not sent.
     * This redirects to the 'login'.
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        return new RedirectResponse(
            '/connect/', // might be the site, where users choose their oauth provider
            Response::HTTP_TEMPORARY_REDIRECT
        );
    }
}