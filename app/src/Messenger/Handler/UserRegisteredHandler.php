<?php

namespace App\Messenger\Handler;

use App\Entity\User;
use App\Messenger\Message\UserRegisteredEvent;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UserRegisteredHandler implements MessageHandlerInterface
{
    /**
     * @var EUserRepository
     */
    private $userRepo;

    public function __construct(UserRepository $repo)
    {
        $this->userRepo = $repo;
    }

    public function __invoke(UserRegisteredEvent $event)
    {
        $userId = $event->getUserId();

        /** @var User $user */
        $user = $this->userRepo->find($userId);

        if (!$user) {
            return;
        }
    }
}