<?php

namespace App\Services;

use App\Entity\ProductImage;
use App\Services\ImageResizer;
use App\Services\FilesystemService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

class FileStorageService
{
    /**
     * @var SluggerInterface
     */
    private $slugger;

    /**
     * @var string
     */
    private $uploadsTempDir;

    /**
     * @var FilesystemService
     */
    private $filesystem;

    /**
     * @var ImageResizer
     */
    private $imageResizer;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(
        SluggerInterface $slugger, 
        FilesystemService $filesystem,
        ImageResizer $imageResizer,
        EntityManagerInterface $em,
        string $uploadsTempDir)
    {
        $this->em = $em;
        $this->slugger = $slugger;
        $this->filesystem = $filesystem;
        $this->imageResizer = $imageResizer;
        $this->uploadsTempDir = $uploadsTempDir;
    }

    /**
     * @param UploadedFile $uploadedFile
     * @return string|null
     */
    public function saveUploadedFileIntoTemp(UploadedFile $uploadedFile): ?string
    {
        $originalFilename = pathinfo($uploadedFile->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);

        $filename = sprintf('%s-%s.%s', $safeFilename, uniqid(), $uploadedFile->guessExtension());

        $this->filesystem->createFolderIfItNotExist($this->uploadsTempDir);

        try {
            $uploadedFile->move($this->uploadsTempDir, $filename);
        } catch (FileException $e) {
            return null;
        }

        return $filename;
    }

    /**
     * @param string $productDir
     * @param string $tempImageFilename
     * @return ProductImage
     */
    public function saveImageForProduct(string $productDir, string $tempImageFilename)
    {
        $this->filesystem->createFolderIfItNotExist($productDir);

        $filenameId = uniqid();
        $imageSmallParams = [
            'width' => 100,
            'height' => null,
            'newFolder' => $productDir,
            'newFilename' => sprintf('%s_%s.jpg', $filenameId, 'small')
        ];
        $imageSmall = $this->imageResizer->resizeImageAndSave($this->uploadsTempDir, $tempImageFilename, $imageSmallParams);

        $imageMiddleParams = [
            'width' => 450,
            'height' => null,
            'newFolder' => $productDir,
            'newFilename' => sprintf('%s_%s.jpg', $filenameId, 'middle')
        ];
        $imageMiddle = $this->imageResizer->resizeImageAndSave($this->uploadsTempDir, $tempImageFilename, $imageMiddleParams);

        $imageBigParams = [
            'width' => 900,
            'height' => null,
            'newFolder' => $productDir,
            'newFilename' => sprintf('%s_%s.jpg', $filenameId, 'big')
        ];
        $imageBig = $this->imageResizer->resizeImageAndSave($this->uploadsTempDir, $tempImageFilename, $imageBigParams);

        $productImage = new ProductImage();
        $productImage->setSmallImage($imageSmall);
        $productImage->setMiddleImage($imageMiddle);
        $productImage->setBigImage($imageBig);

        return $productImage;
    }

    /**
     * @param ProductImage $productImage
     * @param string $productDir
     */
    public function removeImageFromProduct(ProductImage $productImage, string $productDir)
    {
        $smallFilePath = $productDir.'/'.$productImage->getSmallImage();
        $this->filesystem->remove($smallFilePath);

        $middleFilePath = $productDir.'/'.$productImage->getMiddleImage();
        $this->filesystem->remove($middleFilePath);

        $bigFilePath = $productDir.'/'.$productImage->getBigImage();
        $this->filesystem->remove($bigFilePath);

        $product = $productImage->getProduct();
        $product->removeProductImage($productImage);

        $this->em->flush();
    }
} 