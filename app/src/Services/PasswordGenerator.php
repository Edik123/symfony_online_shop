<?php

namespace App\Services;

/**
 * @see https://bitbucket.org/Edik123/symfony_blog_api/src/master/blog/src/Security/TokenGenerator.php
 */
class PasswordGenerator
{
    private const ALPHABET = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!-.[]?*()';

    public static function generatePassword(int $length = 8): string
    {
        $password = '';
        $maxNumber = strlen(self::ALPHABET);

        for ($i = 0; $i < $length; $i++) {
            $password .= self::ALPHABET[random_int(0, $maxNumber - 1)];
        }

        return $password;
    }
}