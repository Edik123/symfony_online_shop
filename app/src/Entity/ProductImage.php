<?php

namespace App\Entity;

use App\Repository\ProductImageRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ProductImageRepository::class)
 */
class ProductImage
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Product::class, inversedBy="productImages")
     * @ORM\JoinColumn(nullable=false)
     */
    private $product;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $bigImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $middleImage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $smallImage;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    public function getBigImage(): ?string
    {
        return $this->bigImage;
    }

    public function setBigImage(string $bigImage): self
    {
        $this->bigImage = $bigImage;

        return $this;
    }

    public function getMiddleImage(): ?string
    {
        return $this->middleImage;
    }

    public function setMiddleImage(string $middleImage): self
    {
        $this->middleImage = $middleImage;

        return $this;
    }

    public function getSmallImage(): ?string
    {
        return $this->smallImage;
    }

    public function setSmallImage(string $smallImage): self
    {
        $this->smallImage = $smallImage;

        return $this;
    }
}
