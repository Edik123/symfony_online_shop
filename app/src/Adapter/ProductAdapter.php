<?php

namespace App\Adapter;

use App\DTO\ProductDTO;
use App\Entity\Product;

class ProductAdapter
{
    /**
     * @param Product $product
     *
     * @return ProductDTO|null
     */
    public static function ConvertToProductDTO(?Product $product): ?ProductDTO
    {
        $productDTO = new ProductDTO();

        if (!$product) return $productDTO;
        
        $productDTO->id = $product->getId();
        $productDTO->title = $product->getTitle();
        $productDTO->price = $product->getPrice();
        $productDTO->quantity = $product->getQuantity();
        $productDTO->description = $product->getDescription();
        $productDTO->category = $product->getCategory();
        $productDTO->isPublished = $product->getIsPublished();
        $productDTO->isDeleted = $product->getIsDeleted();

        return $productDTO;
    }

    /**
     * @param Product $product
     * @param ProductDTO $productDTO
     *
     * @return Product
     */
    public static function convertToProduct(Product $product, ProductDTO $productDTO): ?Product
    {
        $product->setTitle($productDTO->title);
        $product->setPrice($productDTO->price);
        $product->setQuantity($productDTO->quantity);
        $product->setDescription($productDTO->description);
        $product->setCategory($productDTO->category);
        $product->setIsPublished($productDTO->isPublished);
        $product->setIsDeleted($productDTO->isDeleted);

        return $product;
    }
}
