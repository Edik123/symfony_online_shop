<?php

namespace App\Adapter;

use App\DTO\CategoryDTO;
use App\Entity\Category;

class CategoryAdapter
{
    /**
     * @param Category|null $category
     * @return CategoryDTO
     */
    public static function ConvertToCategoryDTO(?Category $category): CategoryDTO
    {
        $CategoryDTO = new CategoryDTO();

        if (!$category) return $CategoryDTO;

        $CategoryDTO->id = $category->getId();
        $CategoryDTO->title = $category->getTitle();

        return $CategoryDTO;
    }

    /**
     * @param Category $category
     * @param CategoryDTO $categoryDTO
     *
     * @return Category
     */
    public static function convertToCategory(Category $category, CategoryDTO $categoryDTO): ?Category
    {
        $category->setTitle($categoryDTO->title);

        return $category;
    }
}