<?php

namespace App\DTO;

use App\Entity\Category;
use Symfony\Component\Validator\Constraints as Assert;

class ProductDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @Assert\NotBlank(message="Please enter a title")
     * @var string
     */
    public $title;

    /**
     * @Assert\NotBlank(message="Please enter a price")
     * @Assert\GreaterThanOrEqual(value="0")
     * @var string
     */
    public $price;

    /**
     * @Assert\NotBlank(message="Please indicate the quantity")
     * @var int
     */
    public $quantity;

    /**
     * @var string
     */
    public $description;

    /**
     * @Assert\NotBlank(message="Please select a category")
     * @var Category
     */
    public $category;

    /**
     * @var bool
     */
    public $isPublished;

    /**
     * @var bool
     */
    public $isDeleted;
}