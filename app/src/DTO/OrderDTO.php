<?php

namespace App\Form\DTO;

use App\Entity\User;

class OrderDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var User
     */
    public $owner;

    /**
     * @var int
     */
    public $status;

    /**
     * @var string
     */
    public $totalPrice;

    /**
     * @var \DateTime
     */
    public $createdAt;
}