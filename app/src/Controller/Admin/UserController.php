<?php

namespace App\Controller\Admin;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

/**
 * @Route("/admin")
 */
class UserController extends AbstractController
{
    private $passwordEncoder;
    
    private $em;

    public function __construct(
        UserPasswordHasherInterface $passwordEncoder,
        EntityManagerInterface $em
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->em = $em;
    }
    
    /**
     * @Route("/users", name="admin_user_index")
     */
    public function index(UserRepository $repo, PaginatorInterface $paginator, Request $request): Response
    {
        $users = $repo->findAll();

        $pagination = $paginator->paginate(
            $users,
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin/user/index.html.twig', [
            'pagination' => $pagination
        ]);
    }

    /**
     * Allows you to create a user
     * 
     * @Route("/users/create", name="admin_user_create")
     * 
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $user = new User();

        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $user->setEmail($form->get('email')->getData());
            
            if ($form->get('password')) {
                $plainPassword = $form->get('password')->getData();
                $encodedPassword = $this->passwordEncoder->hashPassword($user, $plainPassword);
                $user->setPassword($encodedPassword);
            }
            
            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash(
                'success',
                "User <b>{$user->getFullName()}</b> was created!"
            );

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to edit a user
     * 
     * @Route("/users/edit/{id}", name="admin_user_edit", requirements={"id"="\d+"})
     * 
     * @param User $user
     * @param Request $request
     * @return Response
     */
    public function edit(User $user, Request $request): Response
    {
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            if ($form->get('password')) {
                $plainPassword = $form->get('password')->getData();
                $encodedPassword = $this->passwordEncoder->hashPassword($user, $plainPassword);
                $user->setPassword($encodedPassword);
            }

            $this->em->persist($user);
            $this->em->flush();

            $this->addFlash(
                'success',
                "The user <strong>{$user->getFullName()}</strong> has been edit!"
            );

            return $this->redirectToRoute('admin_user_index');
        }

        return $this->render('admin/user/edit.html.twig', [
            'user' => $user,
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to delete a user
     *
     * @Route("/users/{id}/delete", name="admin_user_delete")
     *
     * @param User $user
     * @return Response
     */
    public function delete(User $user): Response
    {
        $this->em->remove($user);
        $this->em->flush();
        
        /*$category->setIsDeleted(true);

        foreach($category->getProducts()->getValues() as $product){
            $product->setCategory(null);
        }*/

        $this->addFlash(
            'success',
            "User from {$user->getFullName()} has been deleted!"
        );

        return $this->redirectToRoute('admin_user_index');
    }

    /**
     * @Route("/users/{id}", name="admin_user_show")
     * 
     * @param User $user
     * @return Response
     */
    public function show(User $user): Response
    {
        return $this->render('user/show.html.twig', [
               'user' => $user
        ]);
    }
}