<?php

namespace App\Controller\Admin;

use App\Entity\Product;
use App\Form\ProductType;
use App\Adapter\ProductAdapter;
use App\Handler\ProductImageHandler;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/products")
 */
class ProductController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @Route("/", name="admin_product_index")
     */
    public function index(ProductRepository $repo)
    {
        return $this->render('admin/product/index.html.twig', [
            'products' => $repo->findAll()
        ]);
    }

    /**
     * Allows you to create a product
     * 
     * @Route("/create", name="admin_product_create")
     * 
     * @param Request $request
     * @return Response
     */
    public function create(Request $request, ProductImageHandler $productImageHandler): Response
    {
        $product = new Product();
        $productDTO = ProductAdapter::ConvertToProductDTO($product);

        $form = $this->createForm(ProductType::class, $productDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $product = ProductAdapter::convertToProduct($product, $productDTO);
            $this->em->persist($product);

            $product = $productImageHandler->processEditImage($product, $form);
            $this->em->persist($product);
            $this->em->flush();

            $this->addFlash(
                'success',
                "Product <b>{$product->getTitle()}</b> was created!"
            );

            return $this->redirectToRoute('admin_product_index');
            /*return $this->redirectToRoute('product_show', [
                'id' => $product->getId()
            ]);*/
        }

        return $this->render('admin/product/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to edit a product
     * 
     * @Route("/edit/{id}", name="admin_product_edit", requirements={"id"="\d+"})
     * @Route("/edit", name="admin_product_edit_blank")
     * 
     * @param Product $product
     * @param Request $request
     * @return Response
     */
    public function edit(Product $product, ProductImageHandler $productImageHandler, Request $request): Response
    {
        $productDTO = ProductAdapter::ConvertToProductDTO($product);

        $form = $this->createForm(ProductType::class, $productDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $product = ProductAdapter::convertToProduct($product, $productDTO);
            $product = $productImageHandler->processEditImage($product, $form);
            
            $this->em->persist($product);
            $this->em->flush();

            $this->addFlash(
                'success',
                "The product <strong>{$product->getTitle()}</strong> has been edit!"
            );

            return $this->redirectToRoute('admin_product_index');
            /*return $this->redirectToRoute('product_show', [
                'id' => $product->getId()
            ]);*/
        }

        return $this->render('admin/product/edit.html.twig', [
            'product' => $product,
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to delete a product
     *
     * @Route("/{id}/delete", name="admin_product_delete")
     *
     * @param Product $product
     * @return Response
     */
    public function delete(Product $product): Response
    {
        $this->em->remove($product);
        $this->em->flush();

        $this->addFlash(
            'success',
            "Product from {$product->getTitle()} has been deleted!"
        );

        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * Id parameter conversion via ParamConverter to product
     * 
     * @Route("/{id}", name="admin_product_show")
     * 
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {
        return $this->render('product/show.html.twig', [
               'product' => $product
        ]);
    }
}