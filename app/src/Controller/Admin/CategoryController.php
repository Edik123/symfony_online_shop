<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Adapter\CategoryAdapter;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin")
 */
class CategoryController extends AbstractController
{
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     * @Route("/categories", name="admin_category_index")
     */
    public function index(CategoryRepository $repo): Response
    {
        return $this->render('admin/category/index.html.twig', [
            'categories' => $repo->findAll()
        ]);
    }

    /**
     * Allows you to create a category
     * 
     * @Route("/categories/create", name="admin_category_create")
     * 
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $category = new Category();
        $categoryDTO = CategoryAdapter::ConvertToCategoryDTO($category);

        $form = $this->createForm(CategoryType::class, $categoryDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $category = CategoryAdapter::convertToCategory($category, $categoryDTO);
            $this->em->persist($category);
            $this->em->flush();

            $this->addFlash(
                'success',
                "Category <b>{$category->getTitle()}</b> was created!"
            );

            return $this->redirectToRoute('admin_category_index');
            /*return $this->redirectToRoute('product_show', [
                'id' => $product->getId()
            ]);*/
        }

        return $this->render('admin/category/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to edit a category
     * 
     * @Route("/categories/edit/{id}", name="admin_category_edit", requirements={"id"="\d+"})
     * 
     * @param Category $category
     * @param Request $request
     * @return Response
     */
    public function edit(Category $category, Request $request): Response
    {
        $categoryDTO = CategoryAdapter::ConvertToCategoryDTO($category);

        $form = $this->createForm(CategoryType::class, $categoryDTO);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $category = CategoryAdapter::convertToCategory($category, $categoryDTO);
            $this->em->persist($category);
            $this->em->flush();

            $this->addFlash(
                'success',
                "The category <strong>{$category->getTitle()}</strong> has been edit!"
            );

            return $this->redirectToRoute('admin_category_index');
            /*return $this->redirectToRoute('product_show', [
                'id' => $product->getId()
            ]);*/
        }

        return $this->render('admin/category/edit.html.twig', [
            'category' => $category,
            'form' => $form->createView()
        ]);
    }

    /**
     * Allows you to delete a category
     *
     * @Route("/categories/{id}/delete", name="admin_category_delete")
     *
     * @param Caregory $category
     * @return Response
     */
    public function delete(Category $category): Response
    {
        $this->em->remove($category);
        $this->em->flush();
        
        /*$category->setIsDeleted(true);

        foreach($category->getProducts()->getValues() as $product){
            $product->setCategory(null);
        }*/

        $this->addFlash(
            'success',
            "Category from {$category->getTitle()} has been deleted!"
        );

        return $this->redirectToRoute('admin_category_index');
    }

    /**
     * @Route("/categories/{id}", name="admin_category_show")
     * 
     * @param Category $category
     * @return Response
     */
    public function show(Category $category): Response
    {
        return $this->render('category/show.html.twig', [
               'category' => $category
        ]);
    }
}
