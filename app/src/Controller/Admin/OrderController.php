<?php

namespace App\Controller\Admin;

use App\Entity\Order;
use App\Form\DTO\OrderDTO;
use Doctrine\DBAL\FetchMode;
use App\Form\Admin\OrderType;
use App\Form\OrderFilterType;
use App\Services\OrderService;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Lexik\Bundle\FormFilterBundle\Filter\FilterBuilderUpdater;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/order", name="admin_order_")
 */
class OrderController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var FilterBuilderUpdater
     */
    private $filterBuilderUpdater;

    public function __construct(EntityManagerInterface $em, FilterBuilderUpdater $filterBuilderUpdater)
    {
        $this->em = $em;
        $this->filterBuilderUpdater = $filterBuilderUpdater;
    }
    
    /**
     * @Route("/index", name="index")
     */
    public function index(OrderRepository $repo, PaginatorInterface $paginator, Request $request): Response
    {
        $queryBuilder = $repo
            ->createQueryBuilder('o')
            ->leftJoin('o.owner', 'u');

        $filterForm = $this->createForm(OrderFilterType::class, new OrderDTO());
        $filterForm->handleRequest($request);

        if ($filterForm->isSubmitted()) {
            $this->filterBuilderUpdater->addFilterConditions($filterForm, $queryBuilder);
        }

        $pagination = $paginator->paginate(
            $queryBuilder->getQuery(),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('admin/order/index.html.twig', [
            'pagination' => $pagination,
            'form' => $filterForm->createView(),
            'orderStatusChoices' => OrderService::getOrderStatusChoices()
        ]);
    }

    /**
     * @Route("/create", name="create")
     * 
     * @param Request $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $order = new Order();

        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) 
        {
            $order->setUpdatedAt(new \DateTimeImmutable());
            $this->em->persist($order);
            $this->em->flush();

            $this->addFlash(
                'success',
                "Order <b>{$order->getId()}</b> was created!"
            );

            return $this->redirectToRoute('admin_order_index');
        }

        return $this->render('admin/order/create.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id}", name="edit")
     *
     * @param Order $order
     * @param Request $request
     * @return Response
     */
    public function edit(Order $order, Request $request): Response
    {
        $form = $this->createForm(OrderType::class, $order);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($order);
            $this->em->flush();

            $this->addFlash('success', 'Your changes were saved!');

            return $this->redirectToRoute('admin_order_index', ['id' => $order->getId()]);
        }

        $orderProducts = [];

        /** @var OrderProduct $product */
        foreach ($order->getOrderProducts()->getValues() as $product) {
            $orderProducts[] = [
                'id' => $product->getId(),
                'product' => [
                    'id' => $product->getProduct()->getId(),
                    'title' => $product->getProduct()->getTitle(),
                    'price' => $product->getProduct()->getPrice(),
                    'quantity' => $product->getProduct()->getQuantity(),
                    'category' => [
                        'id' => $product->getProduct()->getCategory()->getId(),
                        'title' => $product->getProduct()->getCategory()->getTitle()
                    ]
                ],
                'quantity' => $product->getQuantity(),
                'pricePerOne' => $product->getPricePerOne()
            ];
        }

        return $this->render('admin/order/edit.html.twig', [
            'order' => $order,
            'orderProducts' => $orderProducts,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(Order $order): Response
    {
        $this->em->remove($order);
        $this->em->flush();

        $this->addFlash('warning', 'The order was successfully deleted!');

        return $this->redirectToRoute('admin_order_index');
    }
}
