<?php

namespace App\Controller\Admin;

use App\Entity\ProductImage;
use App\Handler\ProductImageHandler;
use App\Services\FileStorageService;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin/product-image", name="admin_product_image_")
 */
class ProductImageController extends AbstractController
{
    /**
     * @Route("/delete/{id}", name="delete")
     */
    public function delete(
        ProductImage $productImage, 
        ProductImageHandler $productImageHandler,
        FileStorageService $fileStorage
    ): Response
    {
        if (!$productImage) {
            return $this->redirectToRoute('admin_product_index');
        }

        $product = $productImage->getProduct();

        $productImagesDir = $productImageHandler->getProductImagesDir($product);
        $fileStorage->removeImageFromProduct($productImage, $productImagesDir);

        return $this->redirectToRoute('admin_product_edit', [
            'id' => $product->getId()
        ]);
    }
}