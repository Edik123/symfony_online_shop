<?php

namespace App\Controller\App;

use Symfony\Component\Routing\Annotation\Route;
use KnpU\OAuth2ClientBundle\Client\ClientRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class AuthInstagramController extends AbstractController
{
    /**
     * Link to this controller to start the "connect" process
     *
     * @Route("/connect/instagram", name="connect_instagram_start")
     */
    public function connectAction(ClientRegistry $clientRegistry)
    {
        return $clientRegistry
            ->getClient('instagram') // key used in config/packages/knpu_oauth2_client.yaml
            ->redirect([], []);
    }

    /**
     * After going to Facebook, you're redirected back here because this is the
     * "redirect_route" you configured in config/packages/knpu_oauth2_client.yaml
     *
     * @Route("/connect/instagram/check", name="connect_instagram_check")
     */
    public function connectCheckAction()
    {     
    }
}