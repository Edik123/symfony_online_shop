<?php

namespace App\Controller\App;

use App\Entity\Cart;
use App\Entity\CartProduct;
use App\Repository\CartRepository;
use App\Repository\ProductRepository;
use App\Repository\CartProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/api", name="api_")
 */
class CartApiController extends AbstractController
{
    /**
     * @Route("/cart", methods="POST", name="cart_save")
     */
    public function save(
        Request $request,
        CartRepository $cartRepository,
        ProductRepository $productRepository, 
        CartProductRepository $cartProductRepository
    ): Response
    {
        $productId = $request->request->get('productId');
        $phpSessionId = $request->cookies->get('PHPSESSID');
        
        $product = $productRepository->findOneBy(['uuid' => $productId]);

        $cart = $cartRepository->findOneBy(['sessionId' => $phpSessionId]);
        if (!$cart) {
            $cart = new Cart();
            $cart->setSessionId($phpSessionId);
        }

        $cartProduct = $cartProductRepository->findOneBy(['cart' => $cart, 'product' => $product]);
        if (!$cartProduct) {
            $cartProduct = new CartProduct();
            $cartProduct->setCart($cart);
            $cartProduct->setQuantity(1);
            $cartProduct->setProduct($product);

            $cart->addCartProduct($cartProduct);
        } else {
            $quantity = $cartProduct->getQuantity() + 1;
            $cartProduct->setQuantity($quantity);
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($cartProduct);
        $entityManager->persist($cart);
        $entityManager->flush();

        return new JsonResponse([
            'success' => true,
            'data' => [
                'test' => 123
            ]
        ]);
    }
}
