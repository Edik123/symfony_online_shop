<?php

namespace App\Controller\App;

use App\Handler\OrderHandler;
use App\Repository\CartRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart_show")
     */
    public function show(Request $request, CartRepository $cartRepository): Response
    {
        $phpSessionId = $request->cookies->get('PHPSESSID');
        $cart = $cartRepository->findOneBy(['sessionId' => $phpSessionId]);

        return $this->render('cart/show.html.twig', [
            'cart' => $cart,
        ]);
    }

    /**
     * @Route("/cart/create", name="cart_create")
     */
    public function create(
        Request $request, 
        CartRepository $cartRepository, 
        OrderHandler $orderHandler): Response
    {
        $phpSessionId = $request->cookies->get('PHPSESSID');
        $user = $this->getUser();

        $cart = $cartRepository->findOneBy(['sessionId' => $phpSessionId]);
        if ($cart) {
            $orderHandler->createOrderFromCart($cart, $user);
        }

        $this->addFlash('success', "Order was created!");

        return $this->redirectToRoute('cart_show');
    }
}
