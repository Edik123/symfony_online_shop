<?php

namespace App\Controller\App;

use App\Entity\Category;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class CategoryController extends AbstractController
{
    /**
     * 
     * @Route("/category/{slug}", name="category_show")
     * 
     * @param Category $category
     * @return Response
     */
    public function show(Category $category): Response
    {
        if (!$category) {
            throw new NotFoundHttpException();
        }

        $products = $category->getProducts()->getValues();
        
        return $this->render('category/show.html.twig', [
               'category' => $category,
               'products' => $products
        ]);
    }
}