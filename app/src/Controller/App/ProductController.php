<?php

namespace App\Controller\App;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProductController extends AbstractController
{
    /**
     * Id parameter conversion via ParamConverter to product
     * 
     * @Route("/product/{uuid}", name="product_show")
     * 
     * @param Product $product
     * @return Response
     */
    public function show(Product $product): Response
    {
        if (!$product) {
            throw new NotFoundHttpException();
        }

        return $this->render('product/show.html.twig', [
               'product' => $product
        ]);
    }

    public function showSimilarProducts(ProductRepository $productRepository, int $productCount = 2, int $categoryId = null): Response
    {
        $params = [];
        if ($categoryId) {
            $params['category'] = $categoryId;
        }

        $products = $productRepository->findBy($params, ['id' => 'DESC'], $productCount);

        return $this->render('partials/similar_products.html.twig', [
            'products' => $products,
        ]);
    }
}