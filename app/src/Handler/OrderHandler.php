<?php

namespace App\Handler;

use App\Entity\Cart;
use App\Entity\User;
use App\Entity\Order;
use App\Entity\CartProduct;
use App\Entity\OrderProduct;
use App\Services\OrderService;
use Doctrine\ORM\EntityManagerInterface;

class OrderHandler
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {

        $this->entityManager = $entityManager;
    }

    public function createOrderFromCart(Cart $cart, User $user)
    {
        $order = new Order();
        $order->setOwner($user);
        $order->setStatus(OrderService::ORDER_STATUS_CREATED);
        $orderTotalPrice = 0;

        /** @var CartProduct $cartProduct */
        foreach ($cart->getCartProducts()->getValues() as $cartProduct) {
            $product = $cartProduct->getProduct();

            $orderProduct = new OrderProduct();
            $orderProduct->setAppOrder($order);
            $orderProduct->setQuantity($cartProduct->getQuantity());
            $orderProduct->setPricePerOne($product->getPrice());
            $orderProduct->setProduct($product);

            $orderTotalPrice += $orderProduct->getQuantity() * $orderProduct->getPricePerOne();

            $order->addOrderProduct($orderProduct);
            $this->entityManager->persist($orderProduct);
        }
        
        $order->setTotalPrice($orderTotalPrice);
        $order->setIsDeleted(true);

        $this->entityManager->persist($order);
        $this->entityManager->flush();
    }
}
