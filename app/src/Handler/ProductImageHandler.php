<?php

namespace App\Handler;

use App\Entity\Product;
use App\Services\FileStorageService;
use Symfony\Component\Form\Form;

class ProductImageHandler
{
    /**
     * @var FileStorage
     */
    private $fileStorage;

    /**
     * @var string
     */
    private $productImagesDir;

    public function __construct(FileStorageService $fileStorage, string $productImagesDir)
    {
        $this->fileStorage = $fileStorage;
        $this->productImagesDir = $productImagesDir;
    }

    public function processEditImage(Product $product, Form $form)
    {
        $newImageFile = $form->get('image')->getData();

        $tempImageFilename = $newImageFile
            ? $this->fileStorage->saveUploadedFileIntoTemp($newImageFile)
            : null;
        
        if (!$tempImageFilename) {
            return $product;
        }

        $this->updateProductImages($product, $tempImageFilename);

        return $product;
    }

    /**
     * @param Product $product
     * @return string
     */
    public function getProductImagesDir(Product $product)
    {
        return sprintf('%s/%s', $this->productImagesDir, $product->getId());
    }

    public function updateProductImages(Product $product, string $tempImageFilename): Product
    {
        $productDir = $this->getProductImagesDir($product);

        $productImage = $this->fileStorage->saveImageForProduct($productDir, $tempImageFilename);
        $productImage->setProduct($product);
        $product->addProductImage($productImage);

        return $product;
    }
}