<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\Product;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private const CATEGORIES = ['jeans', 'hats', 'jackets', 'dresses', 'sneakers'];

    /**
     * @var UserPasswordHasherInterface
     */
    private $passwordHasher;

    private $faker;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
        $this->faker = \Faker\Factory::create();
    }

    public function load(ObjectManager $manager): void
    {
        $this->loadUsers($manager);
        $this->loadProducts($manager);

        $manager->flush();
    }

    public function loadUsers(ObjectManager $manager)
    {
        $admin = new User();
        $admin->setEmail('ed_va@mail.ru');

        $admin->setPassword(
            $this->passwordHasher->hashPassword(
                $admin,
                'test123'
            )
        );
        $admin->setRoles(['ROLE_ADMIN']);
        $admin-> setIsVerified(true);

        $manager->persist($admin);

        $user = new User();
        $user->setEmail('test_user_1@gmail.com');

        $user->setPassword(
            $this->passwordHasher->hashPassword(
                $user,
                'test123'
            )
        );
        $user->setRoles(['ROLE_USER']);
        $user-> setIsVerified(true);

        $manager->persist($user);
    }

    public function loadProducts(ObjectManager $manager)
    {
        $category = (new Category())->setTitle($this->faker->randomElement(SELF::CATEGORIES));
        $manager->persist($category);

        for ($i = 1; $i <= 10; $i++)
        {
            $product = new Product();
            
            $product->setTitle($this->faker->realText(30))
                    ->setPrice(mt_rand(10,100))
                    ->setQuantity(mt_rand(1,5))
                    ->setCategory($category);
        }

        $manager->persist($product);
    }
}
