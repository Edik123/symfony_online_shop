const $ = require('jquery');
require('bootstrap');

global.$ = global.jQuery = $;

require('jquery.easing');
require('chart.js');

require('./admin');
require('./menu');