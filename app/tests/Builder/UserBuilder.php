<?php

namespace App\Tests\Builder;

use App\Entity\User;

class UserBuilder
{
    private $email;
    private $password;

    public function __construct()
    {
        $this->email = 'test@mail.ru';
        $this->password = 'tested';
    }
    
    public function build(): User
    {
        return (new User())->setEmail($this->email)->setPassword($this->password);
    }
}