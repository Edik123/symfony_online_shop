<?php

namespace App\Tests\Functional;

use Facebook\WebDriver\WebDriverBy;
use Symfony\Component\Panther\Client;
use Facebook\WebDriver\Chrome\ChromeOptions;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Panther\PantherTestCase;
use Facebook\WebDriver\Remote\DesiredCapabilities;

class AuthLoginControllerTest extends PantherTestCase
{
    const USER_EMAIL = 'test_user_1@gmail.com';
    CONST USER_PASSWORD = 'test123';

    public function testLogin(): void
    {
        $client = static::createClient();

        $client->request('GET', '/en/login');
        $client->submitForm('Login', [
            'email' => self::USER_EMAIL,
            'password' => self::USER_PASSWORD
        ]);

        $this->assertResponseRedirects('/en/profile', Response::HTTP_FOUND);

        $client->followRedirect();

        $this->assertResponseIsSuccessful();
    }

    /*public function testLoginWithPantherClient(): void
    {
        $_SERVER['PANTHER_NO_SANDBOX'] = true;
        $client = self::createPantherClient(['browser' => self::CHROME]);

        $client->request('GET', '/en/login');
        $client->submitForm('Login', [
            'email' => self::USER_EMAIL,
            'password' => self::USER_PASSWORD
        ]);

        $this->assertSame(self::$baseUri.'/en/profile', $client->getCurrentURL());

        $this->assertPageTitleContains('Profile');
        $this->assertSelectorTextContains('#page_header_title', 'Welcome to your profile!');
    }*/

    public function testLoginWithSeleniumClient(): void
    {
        $client = $this->initSeleniumClient();

        //$client->request('GET', '/en/login');
        $client->get('GET', '/en/login');
        $client = $client->findElement(WebDriverBy::name("q"));
        $crawler = $client->submit('Login', [
            'email' => self::USER_EMAIL,
            'password' => self::USER_PASSWORD
        ]);

        $this->takeScreenshot($client, 'auth-login-controller-test-login__1');

        $this->assertSame(
            $crawler->filter('#page_header_title')->text(),
            'Welcome, to your profile!'
        );
    }

    protected function takeScreenshot($client, string $filename): void
    {
        $client->takeScreenshot('var/tests-screenshots/'.$filename.'.png');
    }

    protected function initSeleniumClient()
    {
        self::createPantherClient();
        self::startWebServer();

        $capabilities = $this->getChromeCapabilities();

        //return Client::createSeleniumClient('http://selenium:4444/wd/hub', $capabilities, 'http://127.0.0.1:8080');
        return RemoteWebDriver::create('http://selenium:4444/wd/hub', $capabilities);
    }

    private function getChromeCapabilities(): DesiredCapabilities
    {
        $chromeOptions = $this->getChromeOptions();

        $capabilities = DesiredCapabilities::chrome();
        $capabilities->setCapability(ChromeOptions::CAPABILITY, $chromeOptions);

        return $capabilities;
    }

    private function getChromeOptions(): ChromeOptions
    {
        $chromeOptions = new ChromeOptions();
        $chromeOptions->addArguments([
            '--window-size=1320,900',
            '--disable-dev-shm-usage',
            '--no-sandbox',
            '--disable-gpu',
            //'--headless',
            '--verbose',
        ]);
        /*$chromeOptions->addArguments([
            '--remote-debugging-port=9222',
            '--window-size=1920,1080',
            '--disable-dev-shm-usage',
            '--no-sandbox',
            '--disable-gpu',
            'headless',
            'start-maximized',
            '--disable-extensions',
            '--profile-directory=Default',
            '--user-data-dir=~/.config/google-chrome'
        ]);*/

        return $chromeOptions;
    }
}