<?php

namespace App\Tests\Functional\ApiPlatform;

use App\Repository\UserRepository;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductResourceTest extends WebTestCase
{
    /**
     * @var string
     */
    protected $uriKey = '/api/products';

    protected const REQUEST_HEADERS = [
        'HTTP_ACCEPT' => 'application/ld+json',
        'CONTENT_TYPE' => 'application/json'
    ];

    protected const REQUEST_HEADERS_PATCH = [
        'HTTP_ACCEPT' => 'application/ld+json',
        'CONTENT_TYPE' => 'application/merge-patch+json'
    ];

    public function testGetProducts()
    {
        $client = self::createClient();

        $client->request('GET', $this->uriKey, [], [], self::REQUEST_HEADERS);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testGetProduct()
    {
        $client = self::createClient();

        $product = static::getContainer()->get(ProductRepository::class)->findOneBy([]);
        
        $uri = $this->uriKey.'/'.$product->getUuid();

        $client->request('GET', $uri, [], [], self::REQUEST_HEADERS);

        $this->assertResponseStatusCodeSame(200);
    }

    public function testCreateProduct()
    {
        $client = self::createClient();

        $this->checkDefaultUserHasNotAccess($client, $this->uriKey, 'POST');

        $client = self::createClient();

        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => 'ed_va@mail.ru']);

        $client->loginUser($user, 'main');

        $context = [
            'title' => 'New Product',
            'price' => '100',
            'quantity' => 5
        ];

        $client->request('POST', $this->uriKey, [], [], self::REQUEST_HEADERS, json_encode($context));

        $this->assertResponseStatusCodeSame(201);
    }

    public function testPatchProduct()
    {
        $client = self::createClient();

        $product = static::getContainer()->get(ProductRepository::class)->findOneBy([]);

        $uri = $this->uriKey.'/'.$product->getUuid();

        $this->checkDefaultUserHasNotAccess($client, $uri, 'PATCH');

        $client = self::createClient();

        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => 'ed_va@mail.ru']);

        $client->loginUser($user, 'main');

        $context = [
            'title' => 'Updated Product',
        ];

        $client->request('PATCH', $uri, [], [], self::REQUEST_HEADERS_PATCH, json_encode($context));

        $this->assertResponseStatusCodeSame(200);
    }

    public function checkDefaultUserHasNotAccess(KernelBrowser $client, string $uri, string $method)
    {
        $user = static::getContainer()->get(UserRepository::class)->findOneBy(['email' => 'test_user_1@gmail.com']);

        $client->loginUser($user, 'main');

        $client->request($method, $uri, [], [], self::REQUEST_HEADERS, json_encode([]));

        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);

        $client = $this->tearDown();
    }

    public function getResponseDecodedContent(KernelBrowser $client)
    {
        return json_decode(
            $client->getResponse()->getContent()
        );
    }
}