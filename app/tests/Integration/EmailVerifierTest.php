<?php

namespace App\Tests\Integration;

use App\Security\EmailVerifier;
use App\Repository\UserRepository;
use App\Tests\Builder\UserBuilder;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use SymfonyCasts\Bundle\VerifyEmail\VerifyEmailHelperInterface;

class EmailVerifierTest extends KernelTestCase
{
    /**
     * @var VerifyEmailHelperInterface
     */
    private $verifyEmail;

    /**
     * @var EmailVerifier
     */
    private $emailVerifier;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function setUp(): void
    {
        parent::setUp();
        //$kernel = self::bootKernel();

        $this->verifyEmail = static::getContainer()->get(VerifyEmailHelperInterface::class);
        $this->emailVerifier = static::getContainer()->get(EmailVerifier::class);
        //$this->userRepository = self::$container->get(UserRepository::class);
    }

    public function testGenerateEmailSignature(): void
    {
        //$user = $this->userRepository->findOneBy(['email' => 'test@mail.ru']);
        $user = (new UserBuilder())->build();
        
        $currentDateTime = new \DateTimeImmutable();

        $emailSignature = $this->generateSignature($user);

        $this->assertGreaterThan($currentDateTime, $emailSignature->getExpiresAt());
    }

    public function testHandleEmailConfirmation(): void
    {
        $user = (new UserBuilder())->build();

        $emailSignature = $this->generateSignature($user);

        $this->emailVerifier->handleTestEmailConfirmation($emailSignature->getSignedUrl(), $user);
        $this->assertTrue($user->isVerified());
    }

    public function generateSignature($user)
    {
        return $this->verifyEmail->generateSignature(
            'app_verify_email', 
            (string)($user->getId()), $user->getEmail(), ['id' => $user->getId()]
        );
    }
}
